package com.lrgt.maestria.ventaautomoviles.vista;

import com.lrgt.maestria.ventaautomoviles.modelos.*;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alumno on 13/07/2017.
 */
@SpringUI
public class Principal extends UI {

    @Autowired
    private EmpresaRepository empresaRepository;
    @Autowired
    private EmpleadoRepository empleadoRepository;
    @Autowired
    private VehiculoRepository vehiculoRepository;

    private Empresa empresa;
    private Grid<Empleado> empleadoGrid;
    private Grid<Vehiculo> vehiculoGrid;
    private Label promedioEdadEmpleados = new Label();
    private Grid<Vehiculo> toyotaGrid = new Grid<>();

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        cargarDatos();
        setContent(getEmpleadosComponents());
    }

    private void cargarDatos() {
        empresa = new Empresa();
        empresa.setNombre("Los Autos Locos");
        empresa.setDireccion("Ciudad de Guatemala");
        empresa.setAreaEspecialidad("Vehículos Japoneses");
        empresa.setCantidadEmpleados(10);
        empresa.setFechaCreacion(LocalDateTime.now());
        empresaRepository.save(empresa);

        toyotaGrid.addColumn(Vehiculo::getModelo).setCaption("Modelo");
        toyotaGrid.addColumn(Vehiculo::getPrecio).setCaption("Precio");
        toyotaGrid.addColumn(Vehiculo::getAnioFabricacion).setCaption("Año");
        actualizarDatos();
    }

    private void actualizarDatos(){
        float promedio = 0;
        List<Empleado> directores = empleadoRepository.findByEmpresaIdAndPuesto(empresa.getId(), "Director");
        if(directores.size() > 0){
            int suma = 0;
            for (Empleado e: directores){
                suma += e.getEdad();
            }
            promedio = suma / directores.size();
        }
        promedioEdadEmpleados.setStyleName("h2");
        promedioEdadEmpleados.setValue("Promedio edad de directores: " + promedio);
        toyotaGrid.setItems(vehiculoRepository.findByEmpresaIdAndMarcaAndVendido(empresa.getId(), "Toyota", false));
    }

    private HorizontalLayout getEmpresaComponents() {
        Label titulo = new Label("TOYOTA's");
        titulo.setStyleName("h1");
        return new HorizontalLayout(
                new VerticalLayout(
                        new Label("Empresa: " + empresa.getNombre()),
                        new Label("Dirección: " + empresa.getDireccion()),
                        new Label("Area especialidad: " + empresa.getAreaEspecialidad()),
                        new Label("Cantidad de empleados: " + empresa.getCantidadEmpleados()),
                        new Label("Fecha de creación: " + empresa.getFechaFormat()),
                        promedioEdadEmpleados
                ),
                new VerticalLayout(
                        titulo,
                        toyotaGrid
                )
        );
    }

    private VerticalLayout getEmpleadosComponents() {
        empleadoGrid = new Grid<>();
        empleadoGrid.addColumn(Empleado::getNombre).setCaption("Nombre");
        empleadoGrid.addColumn(Empleado::getPuesto).setCaption("Puesto");
        empleadoGrid.addColumn(Empleado::getEdad).setCaption("Edad");
        empleadoGrid.addColumn(Empleado::getAniosExperiencia).setCaption("Años de experiencia");
        empleadoGrid.setWidth("100%");
        empleadoGrid.setItems(empleadoRepository.findByEmpresaId(empresa.getId()));

        Button button = new Button("Ir a vehículos");
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                setContent(getVehiculosComponents());
            }
        });

        return new VerticalLayout(getEmpresaComponents(), getEmpleadoFormulario(), empleadoGrid, button);
    }

    private VerticalLayout getVehiculosComponents() {
        vehiculoGrid = new Grid<>();
        vehiculoGrid.addColumn(Vehiculo::getMarca).setCaption("Marca");
        vehiculoGrid.addColumn(Vehiculo::getModelo).setCaption("Modelo");
        vehiculoGrid.addColumn(Vehiculo::getAnioFabricacion).setCaption("Año");
        vehiculoGrid.addColumn(Vehiculo::getPrecio).setCaption("Precio");
        vehiculoGrid.addColumn(Vehiculo::getVendido).setCaption("Vendido");
        vehiculoGrid.setWidth("100%");
        vehiculoGrid.setItems(vehiculoRepository.findByEmpresaId(empresa.getId()));

        Button button = new Button("Ir a empleados");
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                setContent(getEmpleadosComponents());
            }
        });

        return new VerticalLayout(getEmpresaComponents(), getVehiculoFormulario(), vehiculoGrid, button);
    }

    private HorizontalLayout getEmpleadoFormulario() {
        List<String> puestosList = new ArrayList<>();
        puestosList.add("Director");
        puestosList.add("Secretaria");
        puestosList.add("Vendedor");
        TextField nombre = new TextField("Nombre:");
        ComboBox<String> puesto = new ComboBox<>("Puesto:", puestosList);
        TextField edad = new TextField("Edad:");
        TextField anios = new TextField("Años de experiencia:");
        Button button = new Button("Agregar");
        button.addClickListener(clickEvent -> {
            Empleado empleado = new Empleado();
            empleado.setNombre(nombre.getValue());
            empleado.setPuesto(puesto.getValue());
            empleado.setEdad(Integer.parseInt(edad.getValue()));
            empleado.setAniosExperiencia(Integer.parseInt(anios.getValue()));
            empleado.setEmpresa(empresa);
            empleadoRepository.save(empleado);
            empleadoGrid.setItems(empleadoRepository.findByEmpresaId(empresa.getId()));
            actualizarDatos();

            nombre.clear();
            edad.clear();
            anios.clear();
        });
        HorizontalLayout horizontalLayout = new HorizontalLayout(nombre, puesto, edad, anios, button);
        horizontalLayout.setComponentAlignment(button, Alignment.BOTTOM_CENTER);
        return horizontalLayout;
    }

    private HorizontalLayout getVehiculoFormulario() {
        List<String> marcasList = new ArrayList<>();
        marcasList.add("Toyota");
        marcasList.add("Mitsubishi");
        marcasList.add("Mazda");
        marcasList.add("Honda");
        ComboBox<String> marca = new ComboBox<>("Marca:", marcasList);
        TextField modelo = new TextField("Modelo:");
        TextField anio = new TextField("Año de fabricación:");
        TextField precio = new TextField("Precio:");
        CheckBox vendido = new CheckBox("Vendido", false);
        Button button = new Button("Agregar");
        button.addClickListener(clickEvent -> {
            Vehiculo vehiculo = new Vehiculo();
            vehiculo.setMarca(marca.getValue());
            vehiculo.setModelo(modelo.getValue());
            vehiculo.setAnioFabricacion(Integer.parseInt(anio.getValue()));
            vehiculo.setPrecio(Float.parseFloat(precio.getValue()));
            vehiculo.setVendido(vendido.getValue());
            vehiculo.setEmpresa(empresa);
            vehiculoRepository.save(vehiculo);
            vehiculoGrid.setItems(vehiculoRepository.findByEmpresaId(empresa.getId()));
            actualizarDatos();

            modelo.clear();
            anio.clear();
            precio.clear();
            vendido.setValue(false);
        });
        HorizontalLayout horizontalLayout = new HorizontalLayout(marca, modelo, anio, precio, vendido, button);
        horizontalLayout.setComponentAlignment(vendido, Alignment.MIDDLE_CENTER);
        horizontalLayout.setComponentAlignment(button, Alignment.BOTTOM_CENTER);
        return horizontalLayout;
    }
}
