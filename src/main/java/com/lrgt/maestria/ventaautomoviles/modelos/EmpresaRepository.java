package com.lrgt.maestria.ventaautomoviles.modelos;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by alumno on 13/07/2017.
 */
public interface EmpresaRepository extends JpaRepository<Empresa, Long> {
}
