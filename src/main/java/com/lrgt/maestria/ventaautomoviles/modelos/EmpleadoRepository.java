package com.lrgt.maestria.ventaautomoviles.modelos;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by alumno on 13/07/2017.
 */
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {
    List<Empleado> findByEmpresaId(Long id);
    List<Empleado> findByEmpresaIdAndPuesto(Long id, String puesto);
}
