package com.lrgt.maestria.ventaautomoviles.modelos;

import javax.persistence.*;

/**
 * Created by alumno on 13/07/2017.
 */
@Entity
public class Vehiculo {
    @Id@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String marca, modelo;
    private int anioFabricacion;
    private float precio;
    private boolean vendido;
    @ManyToOne
    private Empresa empresa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAnioFabricacion() {
        return anioFabricacion;
    }

    public void setAnioFabricacion(int anioFabricacion) {
        this.anioFabricacion = anioFabricacion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public boolean isVendido() {
        return vendido;
    }

    public void setVendido(boolean vendido) {
        this.vendido = vendido;
    }

    public String getVendido(){
        return isVendido() ? "Vendido" : "No vendido";
    }
}
