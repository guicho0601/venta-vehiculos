package com.lrgt.maestria.ventaautomoviles.modelos;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by alumno on 13/07/2017.
 */
public interface VehiculoRepository extends JpaRepository<Vehiculo, Long> {
    List<Vehiculo> findByEmpresaId(Long id);
    List<Vehiculo> findByEmpresaIdAndMarcaAndVendido(Long id, String marca, boolean vendido);
}
